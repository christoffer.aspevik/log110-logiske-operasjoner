// javascript version of LOG110key.py

// This is a javascript version of the python program LOG110key.py


// class App:
//     def __init__(self, master):
//         frame = Frame(master)
//         frame.master.title("Setningsoperatorer")
//         frame.pack()

//         self.button1 = Button(frame, text="not / negasjon", command=self.not_)
//         self.button1.pack(side=LEFT)

//         self.button2 = Button(frame, text="and / konjunksjon", command=self.and_)
//         self.button2.pack(side=LEFT)

//         self.button3 = Button(frame, text="or / disjunksjon", command=self.or_)
//         self.button3.pack(side=LEFT)

//         self.button4 = Button(frame, text="implies / kondisjonal", command=self.implies_)
//         self.button4.pack(side=LEFT)

//         self.button5 = Button(frame, text="bikondisjonal", command=self.iff_)
//         self.button5.pack(side=LEFT)

//         self.button6 = Button(frame, text="alfa", command=self.alfa_)
//         self.button6.pack(side=LEFT)

//         self.button7 = Button(frame, text="beta", command=self.beta_)
//         self.button7.pack(side=LEFT)

//         self.button6 = Button(frame, text="lukk", command=frame.quit)
//         self.button6.pack(side=LEFT)

//     def not_(self):
//         root.clipboard_clear()
//         root.clipboard_append("¬")

//     def and_(self):
//         root.clipboard_clear()
//         root.clipboard_append("∧")

//     def or_(self):
//         root.clipboard_clear()
//         root.clipboard_append("∨")

//     def implies_(self):
//         root.clipboard_clear()
//         root.clipboard_append("→")

//     def iff_(self):
//         root.clipboard_clear()
//         root.clipboard_append("↔")

//     def alfa_(self):
//         root.clipboard_clear()
//         root.clipboard_append("α")
    
//     def beta_(self):
//         root.clipboard_clear()
//         root.clipboard_append("β")

// root = Tk()
// app = App(root)
// root.mainloop()

// # -*- coding: utf-8 -*-
// Dette programmet inneholder en rekke funksjoner for å kunne skrive symboler i LOG110.
// Programmet er ment som en hjelp til å skrive oppgaver i LOG110.
// Det skrives i javascript og kjøres i nettleseren.
function not_() {
    document.getElementById("text").value += "¬";
}

function and_() {
    document.getElementById("text").value += "∧";
}

function or_() {
    document.getElementById("text").value += "∨";
}

function implies_() {
    document.getElementById("text").value += "→";
}

function iff_() {
    document.getElementById("text").value += "↔";
}

function alfa_() {
    document.getElementById("text").value += "α";
}

function beta_() {
    document.getElementById("text").value += "β";
}

function clear_() {
    document.getElementById("text").value = "";
}

function copy_() {
    document.getElementById("text").select();
    document.execCommand("copy");
}

function paste_() {
    document.getElementById("text").value += document.execCommand("paste");
}
