from tkinter import *

class App:
    def __init__(self, master):
        frame = Frame(master)
        frame.master.title("Setningsoperatorer")
        frame.pack()

        self.button1 = Button(frame, text="not / negasjon", command=self.not_)
        self.button1.pack(side=LEFT)

        self.button2 = Button(frame, text="and / konjunksjon", command=self.and_)
        self.button2.pack(side=LEFT)

        self.button3 = Button(frame, text="or / disjunksjon", command=self.or_)
        self.button3.pack(side=LEFT)

        self.button4 = Button(frame, text="implies / kondisjonal", command=self.implies_)
        self.button4.pack(side=LEFT)

        self.button5 = Button(frame, text="bikondisjonal", command=self.iff_)
        self.button5.pack(side=LEFT)

        self.button6 = Button(frame, text="alfa", command=self.alfa_)
        self.button6.pack(side=LEFT)

        self.button7 = Button(frame, text="beta", command=self.beta_)
        self.button7.pack(side=LEFT)

        self.button8 = Button(frame, text="eksistensiell kvantor", command=self.kvantE_)
        self.button8.pack(side=LEFT)

        self.button9 = Button(frame, text="universell kvantor", command=self.kvantU_)
        self.button9.pack(side=LEFT)

        self.button10 = Button(frame, text="lukk", command=frame.quit)
        self.button10.pack(side=LEFT)

        # keybindings
        def key(event=None):
            print()
            if event.char == "n":
                self.not_()
            elif event.char == "o":
                self.or_()
            elif event.char == "i":
                self.implies_()
            elif event.char == "a":
                self.alfa_()
            elif event.char == "b":
                self.beta_()
            elif event.char == "e":
                self.kvantE_()
            elif event.char == "u":
                self.kvantU_()
            elif event.char == "q":
                root.destroy()

        # bind keys to functions
        root.bind("<Key>", key)
        # bind shift + key to functions
        root.bind(
            "<Shift-KeyPress>", lambda e: self.and_() if (e.char=="A") else self.iff_() if (e.char=="I") else None
        )

    def not_(self):
        root.clipboard_clear()
        root.clipboard_append("¬")

    def and_(self):
        root.clipboard_clear()
        root.clipboard_append("∧")

    def or_(self):
        root.clipboard_clear()
        root.clipboard_append("∨")

    def implies_(self):
        root.clipboard_clear()
        root.clipboard_append("→")

    def iff_(self):
        root.clipboard_clear()
        root.clipboard_append("↔")

    def alfa_(self):
        root.clipboard_clear()
        root.clipboard_append("α")
    
    def beta_(self):
        root.clipboard_clear()
        root.clipboard_append("β")

    def kvantE_(self):
        root.clipboard_clear()
        root.clipboard_append("∃")

    def kvantU_(self):
        root.clipboard_clear()
        root.clipboard_append("∀")

root = Tk()
app = App(root)
root.mainloop()
 








# ----- MacBook Touchbar version -----
# -- Comment out the code above and --
# --  uncomment the code below to   --
# --    use the touchbar version    --
# ------------------------------------

# from tkinter import *
# from PyTouchBar import *




# from PyTouchBar import *
# from tkinter import *
# class App:
#     def __init__(self, master):
#         frame = Frame(master)
#         frame.master.title("Setningsoperatorer")
#         frame.pack()

        # self.button1 = Button(frame, text="not / negasjon", command=self.not_)
        # self.button1.pack(side=LEFT)

        # self.button2 = Button(frame, text="and / konjunksjon", command=self.and_)
        # self.button2.pack(side=LEFT)

        # self.button3 = Button(frame, text="or / disjunksjon", command=self.or_)
        # self.button3.pack(side=LEFT)

        # self.button4 = Button(frame, text="implies / kondisjonal", command=self.implies_)
        # self.button4.pack(side=LEFT)

        # self.button5 = Button(frame, text="bikondisjonal", command=self.iff_)
        # self.button5.pack(side=LEFT)

        # self.button6 = Button(frame, text="alfa", command=self.alfa_)
        # self.button6.pack(side=LEFT)

        # self.button7 = Button(frame, text="beta", command=self.beta_)
        # self.button7.pack(side=LEFT)

        # self.button6 = Button(frame, text="lukk", command=frame.quit)
        # self.button6.pack(side=LEFT)
        # button = PyTouchBar.TouchBarItems.Button(title = 'Click me!', action = function)
    # def not_():
    #     root.clipboard_clear()
    #     root.clipboard_append("¬")

    # def and_():
    #     root.clipboard_clear()
    #     root.clipboard_append("∧")

    # def or_():
    #     root.clipboard_clear()
    #     root.clipboard_append("∨")

    # def implies_():
    #     root.clipboard_clear()
    #     root.clipboard_append("→")

    # def iff_():
    #     root.clipboard_clear()
    #     root.clipboard_append("↔")

    # def alfa_():
    #     root.clipboard_clear()
    #     root.clipboard_append("α")

    # def beta_():
    #     root.clipboard_clear()
    #     root.clipboard_append("β")

    # def quit_():
    #     root.quit()

    # button = TouchBarItems.Button(title="not / negasjon", action=not_())
# root = Tk()
# app = App(root)
# root.mainloop()
# root.withdraw()
# from PyTouchBar import *
# from tkinter import *

# class App:
#     def __init__(self, master):
#         frame = Frame(master)
#         frame.master.title("Setningsoperatorer")
#         frame.pack()
#         def not_():
#             root.clipboard_clear()
#             root.clipboard_append("¬")

#         def and_():
#             root.clipboard_clear()
#             root.clipboard_append("∧")

#         def or_():
#             root.clipboard_clear()
#             root.clipboard_append("∨")

#         def implies_():
#             root.clipboard_clear()
#             root.clipboard_append("→")

#         def iff_():
#             root.clipboard_clear()
#             root.clipboard_append("↔")

#         def alfa_():
#             root.clipboard_clear()
#             root.clipboard_append("α")

#         def beta_():
#             root.clipboard_clear()
#             root.clipboard_append("β")

#         def quit_():
#             root.quit()
#         # items = [
#         #     TouchBarItems.Button(title="not / negasjon", action=not_()),
#         #     TouchBarItems.Button(title="and / konjunksjon", action=and_()),
#         #     TouchBarItems.Button(title="or / disjunksjon", action=or_()),
#         #     TouchBarItems.Button(title="implies / kondisjonal", action=implies_()),
#         #     TouchBarItems.Button(title="bikondisjonal", action=iff_)(),
#         #     TouchBarItems.Button(title="alfa", action=alfa_()),
#         #     TouchBarItems.Button(title="beta", action=beta_()),
#         #     TouchBarItems.Button(title="lukk", action=quit_())
#         # ]



# # Use the PyTouchBar Python module to create a Touch Bar with the items above
#         TouchBarItems.Button(title = 'Click me!', action = beta_())

# TouchBarItems.Button(title="not / negasjon", action=not_()),
# TouchBarItems.Button(title="and / konjunksjon", action=and_()),
# TouchBarItems.Button(title="or / disjunksjon", action=or_()),
# TouchBarItems.Button(title="implies / kondisjonal", action=implies_()),
# TouchBarItems.Button(title="bikondisjonal", action=iff_)(),
# TouchBarItems.Button(title="alfa", action=alfa_()),
# TouchBarItems.Button(title="beta", action=beta_()),
# TouchBarItems.Button(title="lukk", action=quit_())

# root = Tk()
# app = App(root)
# root.mainloop()
# root.withdraw()

# def not_():
#     print("hi")
# button = TouchBarItems.Button(title="not / negasjon", action=not_())
# def function(picker):
#     print("Color:", picker.color)
# cpk = TouchBarItems.ColorPicker(action=function, type = TouchBarItems.ColorPicker.Type.color)
# root = Tk()
# def function(slider):
#   print ('Value:', slider.value * 100)

# slider = TouchBar.TouchBarItems.Slider(action = function)
# TouchBar.prepare_tk_windows(root)
# # lbl = Label(root, text="Button")
# # lbl.pack()
# root.mainloop()